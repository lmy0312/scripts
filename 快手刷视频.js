auto()
let times = rawInput("请输入要自动刷的视频次数：","10")  //用户设置刷视频的个数，默认1000
// toast(times)
let width = device.width;
let height = device.height;
setScreenMetrics(width, height);
console.show();  //显示悬浮窗（需要先打开悬浮窗权限）
app.launch("com.kuaishou.nebula");   //打开快手极速版
sleep(10000);    //等待应用打开
console.log("准备就绪！");
toast("ready!");
//console.log(device.width/2)

/*核心部分开始*/
for(var i = 0; i<times; i++){

    /*定义随机坐标*/
    let bottomX = width / 2 - (Math.floor(Math.random() * 50) + 1);
    let bottomY = height - (Math.floor(Math.random() * 30) + 500);
    let topX = width / 2 - (Math.floor(Math.random() * 50) + 1);
    let topY = 200 + (Math.floor(Math.random() * 20) + 1);  

    /*下一个*/        
    nextVideo(bottomX, bottomY, topX, topY,(Math.floor(Math.random() * 1) + 1) * 1000);
    u = i + 1;
    console.log("总任务量："+ times + ";已完成：" + u);

    /*随机回看 */
    j = random(1,70);
    /*再回去看一下*/
    if (j == 1){
        lookBack(bottomX, bottomY, topX, topY,(Math.floor(Math.random() * 1) + 1) * 1000);
        nextTwo(bottomX, bottomY, topX, topY,(Math.floor(Math.random() * 1) + 1) * 1000);
    }

    /*不感兴趣下一个*/
    if (j == 2){
        nextTwo(bottomX, bottomY, topX, topY,(Math.floor(Math.random() * 1) + 1) * 1000);
    }

}
/*核心部分结束 */
/*退出程序 */
/*pautojsconsole.hide();
home();//回到首页
/**--------------- */

/*往下看*/
function nextVideo(x1, y1, x2, y2, duration){
    swipe(x1, y1, x2, y2, duration);                                //下一个视频
    delayTime =(Math.floor(Math.random() * 2) + 10) * 1000;         //这里设置每个视频的观看时间为 10-12 秒之间
    sleep(delayTime);                                               //在视频停留10-12秒
}

/*向上滑动 */
function lookBack(x1, y1, x2, y2, duration){
        console.log("开始往回看一个视频");
        swipe(x1, y2, x2, y1, duration);                            //上一个视频
        delayTime =(Math.floor(Math.random() * 2) + 10) * 1000;     //这里设置每个视频的观看时间为 10-12 秒之间
        sleep(delayTime);                                           //在视频停留 10-12 秒
}
    
/*向下滑动两个 在向回滑动之后执行 */
function nextTwo(x1, y1, x2, y2, duration){
        console.log("开始往下看两个视频");
        swipe(x1, y1, x2, y2, duration);
        delayTime =(Math.floor(Math.random() * 2) + 2) * 1000;     //这里设置每个视频的观看时间为 2-4 秒之间
        sleep(delayTime);                                          //在视频停留2-4秒
        swipe(x1, y1, x2, y2, duration);
}
